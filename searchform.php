<?php
/**
 * The template for displaying search forms in Sommelier
 *
 * @package WordPress
 * @subpackage Somelier
 * @since Sommelier 0.1
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label for="s" class="assistive-text"><?php _e( 'Search for:', 'toolbox' ); ?></label>
		<input type="search" class="field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search', 'toolbox' ); ?>" autocomplete="off" />
		<input type="submit" class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'toolbox' ); ?>" />
	</form>
