== Changelog ==

= 0.3 -05-07-2012 =
* Unificado o framework Toolbox e o tema do blog do Sommelier em um só
  - Obs: pode causar dificuldades de atualizações futuras mas resolve nosso problema
  com as traduções do tema
* Adicionada screenshot nova!

= 0.2 - 05-07-2012 =
* Estilização básica de botões de redes sociais
* Imprima ou Compartilhe por email
* Criada folha de estilo optimizada para impressão
* Melhorias nos comentários
* Outras pequenas correções e melhorias

= 0.1.2 - 26-06-2012 =
* Corrigido problemas com o formulário de busca na página de erro 404
* Ajustadas as margens da coluna principal e lateral

= 0.1.1 - 26-06-2012 =
* Corrigido minify do CSS

= 0.1 - 26-06-2012 =
* Primeira versão do novo tema do blog do Sommelier.
* Estrutura limpa mas sólida para customização.
* Utilizando o Toolbox como tema base.