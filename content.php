<?php
/**
 * @package Sommelier
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'toolbox' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<div class="entry-meta">
			<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
				<?php toolbox_posted_on(); ?>
				<span class="sep"><?php
					/* translators: separator used between entry's meta groups */
					_e( ' &middot; ', 'toolbox' ); ?>
				</span>
				<?php
					/* translators: used between list items, there is a space after the comma */
					$categories_list = get_the_category_list( __( ', ', 'toolbox' ) );
					if ( $categories_list && toolbox_categorized_blog() ) :
				?>
				<span class="cat-links">
					<?php printf( __( 'Posted in %1$s', 'toolbox' ), $categories_list ); ?>
				</span>
				<span class="sep"><?php
					/* translators: separator used between entry's meta groups */
					_e( ' &middot; ', 'toolbox' ); ?>
				</span>
				<?php endif; // End if categories ?>

			<?php endif; // End if 'post' == get_post_type() ?>

			<?php if ( comments_open() || ( '0' != get_comments_number() && ! comments_open() ) ) : ?>
			<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'toolbox' ), __( '1 Comment', 'toolbox' ), __( '% Comments', 'toolbox' ) ); ?></span>
			<?php endif; ?>

			<?php edit_post_link( __( 'Edit', 'toolbox' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<?php if ( has_post_thumbnail() && is_home() ) : ?>
	<div class="entry-featured-image">
		<a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_post_thumbnail(); ?></a>
	</div><!-- .entry-thumbnail -->
	<?php endif; ?>

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php

		$custom_more = get_post_meta( $post->ID, 'leia_mais', true );

		if ( ! $custom_more ) : 
			the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'toolbox' ) );
		else :
			the_content( $custom_more );
		endif;
		?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'toolbox' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
